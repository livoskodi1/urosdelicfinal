import { Injectable } from "@angular/core";
import { ContactsService } from "./contacts.service";

@Injectable()
export class ModelService {
  contacts = [];

  constructor(private service: ContactsService) {
    this.refreshContacts();
  }

  refreshContacts() {
    this.service.getContactsObservable().subscribe(contacts => {
      this.contacts = contacts;
    });
  }
  refreshContactWithClbk(clbk) {
    this.service.getContactsObservable().subscribe(contacts => {
      this.contacts = contacts;
      clbk();
    });
    
  }
  addContacts(contacts, clbk) {
    this.service.addContactsObservable(contacts).subscribe(contacts => {
      this.contacts.push(contacts);
      clbk();
    });
  }

  getContactsById(id, clbk) {
    for (var i = 0; i < this.contacts.length; i++) {
      if (this.contacts[i].id == id) {
        clbk(this.contacts[i]);
      }
    }
  }
  getSingleContact(id, clbk) {
    if (this.contacts && this.contacts.length > 0) {
      for (var i = 0; i < this.contacts.length; i++) {
        if (parseInt(this.contacts[i].id) === parseInt(id)) {
          clbk(this.contacts[i]);
        }
      }
    } else {
      this.refreshContactWithClbk(() => {
        for (var i = 0; i < this.contacts.length; i++) {
          if (parseInt(this.contacts[i].id) === parseInt(id)) {
            clbk(this.contacts[i]);
          }
        }
      });
    }
  }

  initializeContactsWithCallback(clbk) {
    if (!this.contacts || this.contacts.length < 1) {
      this.service.getContactsObservable().subscribe(contacts => {
        this.contacts = this.contacts;
        clbk();
      });
    } else {
      clbk();
    }
  }
  updateContacts(contacts, clbk) {
    this.service.updateContactsObservable(contacts).subscribe(contacts => {
      this.refreshContacts();
      clbk();
    });
  }
  deleteContact(id, clbk) {
    this.service.deleteContactsObservable(id).subscribe(() => {
      this.refreshContacts();
      clbk();
    });
  }
}
