import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import "rxjs/add/operator/map";
@Injectable()

export class ContactsService {

  constructor(private http: Http) {}

  getContactsObservable() {
    return this.http.get("http://localhost:3000/contacts").map(response => {
      return response.json();
    });
  }

  getSingleContactObservable(id) {
    return this.http.get("http://localhost:3000/contacts/" + id).map(response => {
      return response.json();
    });
  }

  addContactsObservable(contacts) {
    return this.http.post("http://localhost:3000/contacts", contacts).map(response => {
      return response.json();
    });
  }

  updateContactsObservable(contacts) {
    return this.http.patch("http://localhost:3000/contacts/"+contacts.id, contacts).map(response => {
      return response.json();
    });
  }
  deleteContactsObservable(id) {
    return this.http.delete("http://localhost:3000/contacts/" + id).map(response => {
      return response.json();
  });
}
}
