import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ModelService } from "../services/model.service";
import {
  FormGroup,
  Validators,
  FormsModule,
  ReactiveFormsModule,
  FormControl
} from "@angular/forms";
import { DISABLED } from "@angular/forms/src/model";

@Component({
  selector: "app-edit",
  templateUrl: "./edit.component.html"
})
export class EditComponent implements OnInit {
  id;
  firstName;
  lastName;
  birthDate;
  age;
  img;
  editContactForm: FormGroup;
  user;
  constructor(
    public ServiceModel: ModelService,
    private router: Router,
    public activatedRoute: ActivatedRoute,
    public contactsModel: ModelService
  ) {
    this.editContactForm = new FormGroup({
      id: new FormControl(""),
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl("", Validators.required),
      age: new FormControl(""),
      birthDate: new FormControl(""),
      img: new FormControl(""),
    });
  }

  ngOnInit() {
    this.activatedRoute.parent.params.subscribe(params => {
      this.id = params["id"];
      this.ServiceModel.getSingleContact(this.id, obj => {
      
        this.user = obj;
        var date = new Date(obj.birthDate);
        //console.log(date, obj.birthDate);
        let initialValues = {
          ...this.user,
          birthDate: {
            date: {
              day: date.getDate(),
              month: date.getMonth() + 1,
              year: date.getFullYear()
            }
          }
        };
        //delete initialValues['id'];
        // this.forma.setValue(initialValues)
        this.editContactForm.setValue(initialValues);
      });
    });
    this.activatedRoute.parent.params.subscribe(({ id }) => {
      this.id = id;
    });

  /*  this.contactsModel.initializeContactsWithCallback(() => {
      this.setContactsVariables();
    }); 
  } */

 /* setContactsVariables() {
    this.contactsModel.getContactsById(
      this.id,
      ({ firstName, lastName, age, birthDate, img }) => {
        this.firstName = firstName;

        this.lastName = lastName;

        this.age = age;

        this.birthDate = birthDate;
        this.img = img;
      } 
    ); */
  }  
  saveContact() {
    let contact = {
      age: this.editContactForm.value.age,
      birthDate: this.editContactForm.value.birthDate.jsdate,
      img: this.editContactForm.value.img,
      id: this.id 
        }
    console.log(contact)
    this.contactsModel.updateContacts(contact, ()=> {
      this.router.navigate(["/home"]);} );
    }

    
  }

