import { Component } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ModelService } from '../services/model.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html'
})
export class InfoComponent {
id;
firstName;
lastName;
age;
birthDate;
img;
 
constructor(public activatedRoute: ActivatedRoute, public contactsModel: ModelService, public router:Router) {}

ngOnInit() {
  this.activatedRoute.parent.params.subscribe(({id}) => {
    this.id = id;
  });
  this.contactsModel.initializeContactsWithCallback(()=>{
      this.setContactsVars();
  })
}
setContactsVars() {
  this.contactsModel.getContactsById(
    this.id,
    ({ firstName,lastName,birthDate, age, img }) => {
      this.firstName = firstName;
      this.lastName = lastName;
      this.age = age;
      this.birthDate = birthDate;
      this.img = img;
    }
  );
}
deleteContact(id) {
  this.contactsModel.deleteContact(id, ()=>{
    this.router.navigate(["/home"])
  })

}
editContacts(id) {
  this.router.navigate(["/contacts", id, 'edit']);
}


  

}
