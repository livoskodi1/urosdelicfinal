import { Component } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-single-contact',
  templateUrl: './single-contact.component.html'
})
export class SingleContactComponent  {

  id;
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(paramsObject => {
      this.id = paramsObject["id"];
    });
  }
}
