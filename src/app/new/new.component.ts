import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ModelService } from "../services/model.service";
import {
  FormControl,
  FormGroup,
  Validators,
  MinLengthValidator
} from "@angular/forms";
import { IMyDpOptions } from "../../../node_modules/angular4-datepicker/src/my-date-picker";

@Component({
  selector: "app-new",
  templateUrl: "./new.component.html"
})
export class NewComponent {
  
  actualYear;
  yearOfBirth;
  age;
  formSubmited;
  newContactForm: FormGroup;

  myDatePickerOptions: IMyDpOptions = {
    dateFormat: "dd/mm/yyyy",
    openSelectorOnInputClick: true,
    editableDateField: false,
    inline: false
  };

  constructor(
    public ServiceModel: ModelService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.formSubmited = false;
    this.newContactForm = new FormGroup({
      firstName: new FormControl("", Validators.required),
      lastName: new FormControl("", Validators.required),
      age: new FormControl(""),
      birthDate: new FormControl(""),
      img: new FormControl("")
    });
  }

  submitContact() {
    this.formSubmited = true;

    if (this.newContactForm.valid) {
      if (
        this.newContactForm.controls.age.value &&
        this.newContactForm.controls.birthDate.value
      ) {
        this.actualYear = new Date().getFullYear();
        this.yearOfBirth = this.newContactForm.value.birthDate.jsdate.getFullYear();
        this.age = this.newContactForm.controls.age.value;
        if (this.actualYear - this.yearOfBirth == this.age) {
          let contact = {
            firstName: this.newContactForm.value.firstName,
            lastName: this.newContactForm.value.lastName,
            age: this.newContactForm.value.age,
            birthDate: this.newContactForm.value.birthDate.jsdate,
            img: this.newContactForm.value.img,
            id: this.newContactForm.value.id
          };
          delete contact["id"];

          this.ServiceModel.addContacts(contact, () => {
            this.router.navigate(["/home"]);
          });
        } else {
          alert("Age is not correct.");
        }
      } else if (
        this.newContactForm.controls.age.value ||
        this.newContactForm.controls.birthDate.value
      ) {
        let contact = {
          firstName: this.newContactForm.value.firstName,
          lastName: this.newContactForm.value.lastName,
          age: this.newContactForm.value.age,
          birthDate: this.newContactForm.value.birthDate.jsdate,
          img: this.newContactForm.value.img,
          id: this.newContactForm.value.id
        };
        delete contact["id"];

        this.ServiceModel.addContacts(contact, () => {
          this.router.navigate(["/home"]);
        });
      } else {
        alert("Input age or date of birth or both");
      }
    }
  }

  /* addContact() {
    if (this.newContactForm.valid) {
      let contact = { ...this.newContactForm.value };
      delete contact["id"];///
      this.ServiceModel.addContacts(contact, () => {
        this.router.navigate([""]);
      });
    } else {
      alert("Popunite sva polja!");
    }
  } */
}
