import { Component} from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ModelService } from '../services/model.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent{

  constructor(public contactsModel: ModelService, private router: Router) {}

  openContacts(id) {
    this.router.navigate(["/contacts", id]);
  }
  editContacts(id) {
    this.router.navigate(["/contacts", id, 'edit']);
  }
   addContacts(id){
this.router.navigate(["/contacts/new"]);

   }
   deleteContact(id) {
     this.contactsModel.deleteContact(id, ()=>{
       this.router.navigate(["/home"])
     })
 
  }
 

}
