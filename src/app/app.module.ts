import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ContactsComponent } from './contacts/contacts.component';
import { NewComponent } from './new/new.component';
import { SingleContactComponent } from './single-contact/single-contact.component';
import { EditComponent } from './edit/edit.component';
import { InfoComponent } from './info/info.component';
import { HomeComponent } from './home/home.component';
import { ModelService } from './services/model.service';
import { ContactsService } from './services/contacts.service';
import { Http, HttpModule } from '@angular/http';
import { Routes, RouterModule } from "@angular/router";
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SearchPipe } from './search.pipe';
import {MyDatePickerModule} from '../../node_modules/angular4-datepicker/src/my-date-picker/my-date-picker.module';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: "", component:  WelcomeComponent },
  { path: "home", component:  HomeComponent },
  { path: "contacts", component: ContactsComponent },
  { path: "contacts/new", component: NewComponent},
  {
    path: "contacts/:id", component: SingleContactComponent ,
    children: [
      { path: "", redirectTo: "info", pathMatch: "full" },
      { path: "edit", component: EditComponent },
      { path: "info", component: InfoComponent },
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ContactsComponent,
    NewComponent,
    SingleContactComponent,
    EditComponent,
    InfoComponent,
    HomeComponent,
    SearchPipe,
    WelcomeComponent,

  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    FormsModule,
    MyDatePickerModule
    
  ],
  providers: [ModelService, ContactsService],
  bootstrap: [AppComponent]
})
export class AppModule {


 }
