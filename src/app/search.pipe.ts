import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "searchPipe"
}) 

export class SearchPipe implements PipeTransform {

    transform(niz: any[], searchQuery: any): any {
        return searchQuery
          ? niz.filter(contact => {
              return (
                String(contact.firstName)
                  .toLowerCase()
                  .indexOf(searchQuery.toLowerCase()) > -1 ||
                String(contact.lastName)
                  .toLowerCase()
                  .indexOf(searchQuery.toLowerCase()) > -1 ||
                String(contact.age)
                  .toLowerCase()
                  .indexOf(searchQuery.toLowerCase()) > -1
              );
            })
          : niz;
      }

 }